var slots, slot1, slot2, slot3, winLn, startBtn, stopBtn, stopSlotBtns;

function initSlot () {
  slots = document.getElementById ("slots").getElementsByTagName ("li");
  for (i = 1; i <= 3; i++) window ['slot' + i] = slots [i-1];
  winLn = slots [3];
  startBtn = document.getElementById ("startBtn");
  stopBtn = document.getElementById ("stopBtn");
  stopSlotBtns = document.getElementById ("stopSlotBtns").getElementsByTagName ("button");
  resetSlots ();
  resetSlotBtns ();
}

window.setTimeout ("initSlot()", 50);

function resetSlots () {
  winLn.style.visibility = "hidden";
  slot1.className = slot2.className = slot3.className = "pos0";
}

function resetSlotBtns () {
  startBtn.disabled = false;
  stopBtn.disabled = true;
  for (i = 0; i < stopSlotBtns.length; i++) stopSlotBtns [i].disabled = true;
}

function reverseSlotBtns () {
  startBtn.disabled = true;
  stopBtn.disabled = false;
  for (i = 0; i < stopSlotBtns.length; i++) stopSlotBtns [i].disabled = false;
}

function startSlot () {
  resetSlots ();
  resetSlotBtns ();
  reverseSlotBtns ();
  slot1roll = window.setInterval ("rollSlot(slot1)", 100);
  slot2roll = window.setInterval ("rollSlot(slot2)", 100);
  slot3roll = window.setInterval ("rollSlot(slot3)", 100);
}

function rollSlot (slot) {
  var n = slot.className;
  if (n == "pos0") slot.className = "pos4";
  if (n == "pos4") slot.className = "pos3";
  if (n == "pos3") slot.className = "pos2";
  if (n == "pos2") slot.className = "pos1";
  if (n == "pos1") slot.className = "pos0";
}

function stopSlots () {
  for (i = 1; i <= 3; i++)
    if (window ['slot' + i + 'roll']) window ['slot' + i + 'roll'] = window.clearInterval (window ['slot' + i + 'roll']);
  resetSlotBtns ();
}

function stopSlot (i) {
  if (window ['slot' + i + 'roll']) window ['slot' + i + 'roll'] = window.clearInterval (window ['slot' + i + 'roll']);
  getSlotsResult ();
}

function getSlotsResult () {
  if (!slot1roll && !slot2roll && !slot3roll) {
    winLn.style.visibility = "visible";
    var slotResult = (slot1.className == slot2.className && slot2.className == slot3.className) ? "win" : "lose";
    showSlotResult (slotResult);
    resetSlotBtns ();
  }
}

function showSlotResult (type) {
  var message = (type == "win") ? "Vittoria!" : "Stavolta � andata male...";
  alert (message);
}